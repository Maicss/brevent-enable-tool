#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QListWidget>
#include <QTextEdit>
#include <DMainWindow>
#include <DButtonBox>
#include <QStackedWidget>
#include <QMenu>
#include <QAction>
#include <DSpinner>
#include <QLabel>
#include <DMessageManager>
DWIDGET_USE_NAMESPACE
class MainWindow : public DMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QListWidget *listview;
    QTextEdit* out;
    QStackedWidget* stk;
    QMenu* menu;
    QAction* reload;
    QLabel* emptylist;
    void update_dev();
    DSpinner *spinner;
    QPushButton* btn_install;
    QLabel* showlabel;
    void resizeEvent(QResizeEvent *event);
signals:
    void sendMessage(QIcon,QString);
};

#endif // MAINWINDOW_H
