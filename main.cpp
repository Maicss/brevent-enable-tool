#include "mainwindow.h"
#include <DApplication>
DWIDGET_USE_NAMESPACE
int main(int argc, char *argv[])
{
    DApplication a(argc, argv);
    a.setProductIcon(QIcon::fromTheme("brevent-enable-tool"));
    a.setProductName("黑阈激活工具");
    a.setApplicationVersion("2.2.1");
    a.loadTranslator();
    MainWindow w;
    w.show();

    return a.exec();
}
